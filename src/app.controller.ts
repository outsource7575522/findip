import { Controller, Get, Headers, Ip, Query, Redirect, Res } from '@nestjs/common';
import { AppService } from './app.service';

@Controller()
export class AppController {
  constructor(private readonly appService: AppService) {}

  @Get()
  @Redirect()
  getHello(@Res() req, @Query('p') p: string,@Ip() ip,@Headers() header): any {
    try{
    if(p){
    console.log(Date())
    console.log(ip);
    console.log(header)
    const clientIP = req.headers['x-forwarded-for']?.split(',').shift() || req.socket?.remoteAddress;
    console.log(`The client's IP Address is: ${clientIP}`);
    }
    }
    finally{
      return { url: p};
    }
  }
}
